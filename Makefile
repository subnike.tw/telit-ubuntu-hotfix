KVERSION := $(shell uname -r | cut -d'-' -f1)
SOURCE := Ubuntu-telit/Ubuntu-$(KVERSION)

all:
	make -C $(SOURCE)

clean:
	make -C $(SOURCE) clean

install: all
	make -C $(SOURCE) install

.PHONY: all clean install
